# Noitcereon CV 2024

A website CV/Portfolio built with Astro.

> This CV is a work-in-progress, but it can seen via [GitLab Pages](https://noitcereon-cv-2024-noitcereon-658f91340ac7447ccede2ab93c28397fc.gitlab.io/ ). It is based on the [old CV page](https://gitlab.com/Noitcereon/noitcereon-cv) I made in 2022.

## Install

Use below commands for your installation needs.

All commands are run from the root of the project, from a terminal:

| Command                      | Action                                                           |
| :--------------------------- | :--------------------------------------------------------------- |
| `npm install`                | Installs dependencies                                            |
| `npm run build-tailwind-css` | Compiles Tailwind CSS into ./public/assets/compiled-tailwind.css |
| `npm run dev`                | Starts local dev server at `localhost:4321`                      |
| `npm run build`              | Build your production site to `./public/`                        |
| `npm run preview`            | Preview your build locally, before deploying                     |
| `npm run astro ...`          | Run CLI commands like `astro add`, `astro check`                 |
| `npm run astro -- --help`    | Get help using the Astro CLI                                     |

You might also want to take a look at the [Astro documentation](https://docs.astro.build) 

## Usage

This section will eventually contain a link to the live website, once it is up and running.

For now, you can see my old CV site at: https://whoisthomasandersen.com


## Maintainer

@Noitcereon

## Contributions

You can suggest changes via Issues, but as this is intended for myself, I'll generally not accept contributions.

