import { defineConfig } from 'astro/config';

import tailwind from "@astrojs/tailwind";

// https://astro.build/config
export default defineConfig({
  integrations: [tailwind()],
  site: 'https://noitcereon-cv-2024-noitcereon-658f91340ac7447ccede2ab93c28397fc.gitlab.io/',
  base: '',

  // GitLab Pages requires exposed files to be located in a folder called "public".
  // So we're instructing Astro to put the static build output in a folder of that name.
  outDir: 'public',

  // The folder name Astro uses for static files (`public`) is already reserved 
  // for the build output. So in deviation from the defaults we're using a folder
  // called `static` instead.
  publicDir: 'static',

  build: {
    // Specifies the directory in the build output where Astro-generated assets (bundled JS and CSS for example) should live.
    assets: 'assets',

    // Control the output file format of each page.
    // Example: Generate `portfolio.html` instead of `portfolio/index.html` during build when set to 'file'
    format: 'file'
  }
});